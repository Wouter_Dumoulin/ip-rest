/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.iprest.controller;

import com.exevan.ipdomain.domain.Game;
import com.exevan.ipdomain.domain.Genre;
import com.exevan.ipdomain.domain.Publisher;
import com.exevan.ipdomain.service.MainService;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Exevan
 */
@RestController
@RequestMapping(value = "/rest")
public class GameRestController {

    @Autowired
    private MainService service;

    public GameRestController() {
        System.out.println("initializing controller");
    }

    @GetMapping("/games")
    public ResponseEntity getAllGames() {
        Collection<Game> games = service.getAllGames();
        if (games.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity(games, HttpStatus.OK);
    }

    @GetMapping("/games/{id}")
    public ResponseEntity getGame(@PathVariable("id") long id) {
        Game game = service.getGame(id);
        if (game == null) {
            System.out.println("User with id " + id + " not found");
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(game, HttpStatus.OK);
    }

    @PostMapping(value = "/games", consumes="application/json")
    public ResponseEntity addGame(@RequestBody Game game) {
        Publisher publisher = service.getPublisher(game.getPublisher().getId());
        game.setPublisher(publisher); 
        return new ResponseEntity(service.addGame(game), HttpStatus.OK);
    }

    @PutMapping("/games/{id}")
    public ResponseEntity updateGame(@PathVariable long id, @RequestBody Game game) {
        return new ResponseEntity(service.updateGame(id, game.getTitle(), game.getGenre(), game.getReleaseDate(), game.getPublisher().getId()), HttpStatus.OK);
    }

    @DeleteMapping("/games/{id}")
    public ResponseEntity deleteGame(@PathVariable long id) {
        service.removeGame(id);
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @GetMapping("/genres")
    public ResponseEntity getAllGenres() {
        Collection<Genre> genres = service.getAllGenres();
        if (genres.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity(genres, HttpStatus.OK);
    }

    @GetMapping("/publishers")
    public ResponseEntity getAllPublishers() {
        Collection<Publisher> publishers = service.getAllPublishers();
        if (publishers.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity(publishers, HttpStatus.OK);
    }
}
