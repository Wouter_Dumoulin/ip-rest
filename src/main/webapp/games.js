/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var app = angular.module('games', []);

app.controller('games', function ($scope, $http, $filter) {
    $scope.releaseDate = $filter("date")(new Date(), 'dd-MM-yyyy');
    getGames($http, $scope);
    getGenres($http, $scope);
    getPublishers($http, $scope);
    $scope.submit = function () {
        var game = {
            id: '0',
            title: $scope.title,
            genre: $scope.genre,
            releaseDate: new Date($scope.releaseDate).getTime(),
            publisher: {
                id: $scope.publisher.id,
                name: ''
            }
        };
        $http.post("http://193.191.187.14:10368/Project_Internetprogrammeren_Rest/rest/games.json", game).then(getGames($http, $scope));
        getGames($http, $scope);
    };
});

function getGames($http, $scope) {
    $http.get("http://193.191.187.14:10368/Project_Internetprogrammeren_Rest/rest/games.json")
            .then(function (response) {
                $scope.games = response.data;
            });
}

function getGenres($http, $scope) {
    $http.get("http://193.191.187.14:10368/Project_Internetprogrammeren_Rest/rest/genres.json")
            .then(function (response) {
                $scope.genres = response.data;
            });
}

function getPublishers($http, $scope) {
    $http.get("http://193.191.187.14:10368/Project_Internetprogrammeren_Rest/rest/publishers.json")
            .then(function (response) {
                $scope.publishers = response.data;
            });
}